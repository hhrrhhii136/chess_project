#include "MainGame.h"

MainGame::MainGame(const char player)
{
	this->_player = player;
}

MainGame::~MainGame()
{
}


map<string, PiceControl*>* MainGame::getAllPices()
{
	return &(this->allPices);
}


void MainGame::SetPlayer(const char player)
{
	this->_player = player;
}

void MainGame::deletePice(string key)
{
	this->allPices.erase(key);
}

void MainGame::addPice(string oldKey, string newKey)
{
	PiceControl* temp(this->allPices[oldKey]);

	temp->SetPlace(newKey);
	temp->SetPlaceOnBoard(convertString(newKey));

	deletePice(oldKey);

	Point p_check_board(convertString(newKey));
	if(board[p_check_board.GetX()][p_check_board.GetY()] != '#')
		deletePice(newKey);
	this->allPices.insert(std::pair<string, PiceControl*>(newKey, temp));
}


void MainGame::GameControl(string msgFromGame)
{
	string picePlace;
	string piceToGo;

	picePlace += msgFromGame[0];
	picePlace += msgFromGame[1];

	piceToGo += msgFromGame[2];
	piceToGo += msgFromGame[3];


	Point picePlacePoint(convertString(picePlace));
	Point piceToGoPoint(convertString(piceToGo));


	char to_save = ' ';
	bool need_to_restore = false;

	if (board[picePlacePoint.GetX()][picePlacePoint.GetY()] == '#')
		throw MoveExeption(MOVE_NUMBER_2);


	else if ((this->_player == 'W') && (!islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()]))
		|| (this->_player == 'B') && (islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()])))
		throw MoveExeption(MOVE_NUMBER_2);

	else if ((picePlacePoint.GetX() < 0 || picePlacePoint.GetX() > LEN_OF_BOARD)
		|| (picePlacePoint.GetY() < 0 || picePlacePoint.GetY() > LEN_OF_BOARD)
		|| (piceToGoPoint.GetX() < 0 || piceToGoPoint.GetX() > LEN_OF_BOARD)
		|| (piceToGoPoint.GetX() < 0 || piceToGoPoint.GetY() > LEN_OF_BOARD))
		throw MoveExeption(MOVE_NUMBER_5);

	else if (picePlacePoint == piceToGoPoint)
		throw MoveExeption(MOVE_NUMBER_7);






	else if ((this->allPices)[picePlace]->checkMove(piceToGoPoint))
	{	
		if (stepOver((this->allPices)[picePlace]->piceWay(piceToGoPoint), this->allPices[picePlace]->GetSoldierSign()))
		{
			throw MoveExeption(MOVE_NUMBER_6);
			//�� ������ �� �����
		}

		else
		{
			if (board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] != '#')
			{
				if ((islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()]) && islower(board[piceToGoPoint.GetX()][piceToGoPoint.GetY()]))
					|| (!islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()]) && !islower(board[piceToGoPoint.GetX()][piceToGoPoint.GetY()])))
				{
					throw MoveExeption(MOVE_NUMBER_3);
				}

				else 
					deletePice(piceToGo);
			}

			if (board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] != '#'
				&& ((this->allPices)[picePlace]->GetSoldierSign() == 'p' || (this->allPices)[picePlace]->GetSoldierSign() == 'P'))
			{
				throw MoveExeption(MOVE_NUMBER_6);
			}

			else
			{
				if (board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] != '#')
				{
					to_save = board[piceToGoPoint.GetX()][piceToGoPoint.GetY()];
					need_to_restore = true;
				}
					//p_check_board
				board[picePlacePoint.GetX()][picePlacePoint.GetY()] = '#';
				board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = (this->allPices)[picePlace]->GetSoldierSign();
				addPice(picePlace, piceToGo);

				
				if (islower((this->allPices)[piceToGo]->GetSoldierSign())) // true = white
				{
					if((this->allPices)[piceToGo]->GetSoldierSign() == 'k')
						whiteKingKey = (this->allPices)[piceToGo]->GetPlace();
					
					if (canEatKing(false))
					{
						board[picePlacePoint.GetX()][picePlacePoint.GetY()] = (this->allPices)[piceToGo]->GetSoldierSign();
						
						if (need_to_restore)
							board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = to_save;

						else
							board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = '#';
						
						addPice(piceToGo, picePlace);
						deletePice(piceToGo);

						if ((this->allPices)[picePlace]->GetSoldierSign() == 'k')
							whiteKingKey = (this->allPices)[picePlace]->GetPlace();

						throw MoveExeption(MOVE_NUMBER_4);
					}
				}
				else
				{

					if ((this->allPices)[piceToGo]->GetSoldierSign() == 'K')
						blackKingKey = (this->allPices)[piceToGo]->GetPlace();

					if (canEatKing(true))
					{
						board[picePlacePoint.GetX()][picePlacePoint.GetY()] = (this->allPices)[piceToGo]->GetSoldierSign();
						if(need_to_restore)
							board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = to_save;

						else 
							board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = '#';
						addPice(piceToGo, picePlace);
						deletePice(piceToGo);

						if ((this->allPices)[picePlace]->GetSoldierSign() == 'K')
						{
							blackKingKey = (this->allPices)[picePlace]->GetPlace(); 
						}

						throw MoveExeption(MOVE_NUMBER_4);
					}
				}

				if (canEatKing(islower((this->allPices)[piceToGo]->GetSoldierSign())))
				{
					if (islower((this->allPices)[piceToGo]->GetSoldierSign())) 
						this->allPices[blackKingKey]->SetThreat(true);
					
					else
						this->allPices[whiteKingKey]->SetThreat(true);

					throw MoveExeption(MOVE_NUMBER_1);
				}

				else
				{
					deletePice(picePlace);

					if (islower((this->allPices)[piceToGo]->GetSoldierSign()))
						this->allPices[blackKingKey]->SetThreat(false);
			
					else
						this->allPices[whiteKingKey]->SetThreat(false);

					throw MoveExeption(MOVE_NUMBER_0);
				}

			}
		}
			
	}

	else if (((this->allPices)[picePlace]->GetSoldierSign() == 'p' || (this->allPices)[picePlace]->GetSoldierSign() == 'P'))
	{
		Pawn p((this->allPices)[picePlace]->GetSoldierSign(), (this->allPices)[picePlace]->GetPlaceOnBoard(), (this->allPices)[picePlace]->GetPlace());
		
		if (p.validEat(piceToGoPoint) && board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] != '#')
		{
			if (islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()]) && islower(board[piceToGoPoint.GetX()][piceToGoPoint.GetY()]))
				throw MoveExeption(MOVE_NUMBER_3);

			else if (!islower(board[picePlacePoint.GetX()][picePlacePoint.GetY()]) && !islower(board[piceToGoPoint.GetX()][piceToGoPoint.GetY()]))
				throw MoveExeption(MOVE_NUMBER_3);

			//deletePice(piceToGo);
			board[picePlacePoint.GetX()][picePlacePoint.GetY()] = '#';
			board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = (this->allPices)[picePlace]->GetSoldierSign();
			addPice(picePlace, piceToGo);

			if (canEatKing(!islower((this->allPices)[piceToGo]->GetSoldierSign())))
			{
				board[picePlacePoint.GetX()][picePlacePoint.GetY()] = (this->allPices)[piceToGo]->GetSoldierSign();
				board[piceToGoPoint.GetX()][piceToGoPoint.GetY()] = '#';
				addPice(piceToGo, picePlace);
				deletePice(piceToGo);

				throw MoveExeption(MOVE_NUMBER_4);

			}

			throw MoveExeption(MOVE_NUMBER_0);
		}

		else
			throw MoveExeption(MOVE_NUMBER_6);
	}

	else
		throw MoveExeption(MOVE_NUMBER_6);
}


bool MainGame::stepOver(vector<Point> blocksWay, char soldierSign) 
{
	if (('n' == soldierSign) || ('N' == soldierSign))
		return false;
	else
	{
		//true ������
		//false �� ������
		for (int i = 0; i < blocksWay.size() - 1; i++) 
		{
			if (board[blocksWay[i].GetX()][blocksWay[i].GetY()] != '#')
				return true;
		}
		return false;
	}
}



bool MainGame::canEatKing(bool whiteOrBlack) 
// true = white   false = black
{
	Point* toSend = new Point();


	if (whiteOrBlack)
		toSend = &(this->allPices[blackKingKey]->GetPlaceOnBoard());

	else
		toSend = &(this->allPices[whiteKingKey]->GetPlaceOnBoard());

	

	for (map<string, PiceControl*>::iterator pice = this->allPices.begin(); pice != this->allPices.end(); ++pice)
	{
		if (islower(pice->second->GetSoldierSign()) && (whiteOrBlack) || !islower(pice->second->GetSoldierSign()) && !(whiteOrBlack)) // islower('r') == true   was ==
		{
			if (pice->second->GetSoldierSign() == 'p' || pice->second->GetSoldierSign() == 'P')
			{
				if(dynamic_cast<Pawn*>(pice->second)->validEat(*toSend))
					if (!stepOver(pice->second->piceWay(*toSend), pice->second->GetSoldierSign())) // true = chess
						return true;
			}

			else
			{
				if (pice->second->checkMove(*toSend))
					if (!stepOver(pice->second->piceWay(*toSend), pice->second->GetSoldierSign()))
						return true;
			}
		}
	}
				
	return false;
}