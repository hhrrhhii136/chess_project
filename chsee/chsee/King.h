#pragma once
#include "PiceControl.h"


class King : public PiceControl
{
public:
	King();
	King(const char& soldierSign, const Point& placeOnBoard, const string& place);
	virtual ~King();

	virtual bool checkMove(const Point& toGo);
	virtual vector<Point> piceWay(const Point& toGo);

	bool GetThreat() const;
	void SetThreat(const bool threat);
private:
	bool _threat;
};