#pragma once
#include <iostream>

#include "Point.h"
using std::string;
class Regular
{
public:
	Regular();
	Regular(string place, Point placeInBoard);

	void printSome();

	bool checkMove(Point move);

private:
	int _numOfMoves;
	string _place;
	Point _placeInBoard;
};