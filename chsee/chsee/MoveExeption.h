#pragma once

#include <iostream>
#include <exception>



#define ERROR_2 2
#define ERROR_3 3
#define ERROR_4 4
#define ERROR_5 5
#define ERROR_6 6
#define ERROR_7 7

class MoveExeption : public std::exception
{
public:
	MoveExeption(int numberOfMove)
	{
		this->_numOfMove = numberOfMove;
	}

	virtual char* what()
	{
		char msgToGraphics[1024];

		msgToGraphics[0] = (char)(this->_numOfMove + '0');
		msgToGraphics[1] = 0;
		return msgToGraphics;
	}
private:
	int _numOfMove;
};